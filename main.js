const playerCoordinate = {
    row: 0,
    cell: 0
}

const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW W WWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW"
]

const mapGrid = document.getElementById('mapGrid')

//building map
for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
    let mapRow = map[rowIndex]
    let row = document.createElement('div')
    row.dataset.rowIndex = rowIndex
    row.className = 'row'

    for (let cellIndex = 0; cellIndex < mapRow.length; cellIndex++) {
        let cell = document.createElement('div')
        cell.dataset.cellIndex = cellIndex
        cell.dataset.rowIndex = rowIndex
        cell.className = 'cell'

        switch (mapRow[cellIndex]) {
            case 'W': cell.classList.add('wall')
            break
            case ' ': cell.classList.add('space')
            break
            case 'S': 
                cell.id = 'start'; 
                playerCoordinate.row = rowIndex; 
                playerCoordinate.cell = cellIndex;
            break
            case 'F': cell.classList.add('finish')
            break
        }
        row.appendChild(cell)
    }
    mapGrid.appendChild(row)
}

//moving the player
function movePlayer (row, cell) {
    
    if (map[row][cell] == 'F'){
        alert('You win')
    } else if (!map[row] || map[row][cell] !== ' '){
        return
    }

    movePlayerModel(row, cell)
    movePlayerView(row, cell)
}

//show where the player is
function movePlayerModel (row, cell) {
    playerCoordinate.row = row
    playerCoordinate.cell = cell
}

//move the player piece visually
function movePlayerView (row, cell) {
    const nextCell = document.querySelector("[data-row-index='" + row + "'][data-cell-index='" + cell + "']")
    nextCell.appendChild(player)
}

//adding keydown events
document.addEventListener('keydown', event => {
    if (!event.key.startsWith('Arrow')) return

    const directions = {
        ArrowLeft: [playerCoordinate.row, playerCoordinate.cell - 1],
        ArrowRight: [playerCoordinate.row, playerCoordinate.cell + 1],
        ArrowDown: [playerCoordinate.row + 1, playerCoordinate.cell],
        ArrowUp: [playerCoordinate.row - 1, playerCoordinate.cell],
    }

    movePlayer(...directions[event.key])

    console.log({ row: playerCoordinate.row, cell: playerCoordinate.cell })
})

//finding the player piece
function playerPiece() {
    let startCell = document.querySelector('#start')
    const player = document.getElementById('player')
    startCell.appendChild(player)

    return player
}

player = playerPiece()